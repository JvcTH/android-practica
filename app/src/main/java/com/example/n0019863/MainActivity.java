package com.example.n0019863;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        RecyclerView rv_contacto = findViewById(R.id.recycler_view);
        rv_contacto.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rv_contacto.setLayoutManager(layoutManager);
        rv_contacto.setAdapter(new adaptadorContacto(getList(),this,MainActivity.this));

    }
    private List<contacto> getList(){

        List<contacto> contactos = new ArrayList<>();

        contacto  contacto1 = new contacto();
        contacto1.nombre = "jose";
        contacto1.numero = "911111111";
        contactos.add(contacto1);

        contacto  contacto2 = new contacto();
        contacto2.nombre = "Messi";
        contacto2.numero = "91111112";
        contactos.add(contacto2);

        contacto  contacto3 = new contacto();
        contacto3.nombre = "Cristiano";
        contacto3.numero = "911111113";
        contactos.add(contacto3);

        contacto  contacto4 = new contacto();
        contacto4.nombre = "Lapadula";
        contacto4.numero = "911111114";
        contactos.add(contacto4);

        contacto  contacto5 = new contacto();
        contacto5.nombre = "CristianCueva";
        contacto5.numero = "911111115";
        contactos.add(contacto5);

        contacto  contacto6 = new contacto();
        contacto6.nombre = "SAla";
        contacto6.numero = "911111115";
        contactos.add(contacto6);

        contacto  contacto7 = new contacto();
        contacto7.nombre = "Paolo";
        contacto7.numero = "911111115";
        contactos.add(contacto7);

        contacto  contacto8 = new contacto();
        contacto8.nombre = "Gallese";
        contacto8.numero = "911111115";
        contactos.add(contacto8);

        contacto  contacto9 = new contacto();
        contacto9.nombre = "Pink";
        contacto9.numero = "911111115";
        contactos.add(contacto9);

        contacto  contacto10 = new contacto();
        contacto10.nombre = "Keiko";
        contacto10.numero = "911111115";
        contactos.add(contacto10);


        return contactos;
    }
}